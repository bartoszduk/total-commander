@echo off
where >nul 2>nul wsl
IF %ERRORLEVEL% NEQ 0 goto :Cygwin
start wsl --cd %1
goto :exit

:Cygwin
copy >nul 2>nul /Y c:\totalcmd\addons\Cygwin\Data\admin\original.bashrc c:\totalcmd\addons\Cygwin\Data\admin\.bashrc
set "path=%1"
set "path=%path:\=/%"

echo | set /p temp=#This is a temporary file. Permanent changes must be made to original.bashrc. >> c:\totalcmd\addons\Cygwin\Data\admin\.bashrc
echo. >> c:\totalcmd\addons\Cygwin\Data\admin\.bashrc
echo | set /p temp=cd %path% >> c:\totalcmd\addons\Cygwin\Data\admin\.bashrc

cygwin-portable-mintty.cmd

goto :exit

:exit
exit